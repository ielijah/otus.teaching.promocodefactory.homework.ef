﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _promocodeRepository = promocodeRepository;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();

            var promocodesModelList = promocodes?.Select(x =>
                new PromoCodeShortResponse()
                {
                    Code = x.Code,
                    BeginDate = x.BeginDate.ToString(),
                    EndDate = x.EndDate.ToString(),
                    Id = x.Id,
                    PartnerName = x.PartnerName,
                    ServiceInfo = x.ServiceInfo,
                })?.ToList()??null;

            return promocodesModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            Guid preferenceId;
            if (!preferences.Any(x => x.Name == request.Preference))
                return NotFound();
            else
                preferenceId = preferences.First(x => x.Name == request.Preference).Id;

            var customers = await _customerRepository.GetAllAsync();
            if(!customers.Any(x=>x.Preferences.Any(y=>y.Name==request.Preference)))
                return NotFound();

            List<PromoCode> promoCodes = new List<PromoCode>();
            foreach(var customer in customers.Where(x => x.Preferences.Any(y => y.Name == request.Preference)))
                await _promocodeRepository.CreateOrUpdateAsync(new PromoCode()
                {
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),
                    Code = request.PromoCode,
                    CustomerId = customer.Id,
                    PartnerManagerId = null,
                    PartnerName = request.PartnerName,
                    PreferenceId = preferenceId,
                    ServiceInfo = request.ServiceInfo
                });

            return Ok();
        }
    }
}