﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Id = x.Id
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить клиента по ID
        /// </summary>
        /// <param name="id">Id для поиска</param>
        /// <returns>Запись о сотруднике</returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                Id = customer.Id,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes?.Select(x =>
                 new PromoCodeShortResponse()
                 {
                     Code = x.Code,
                     BeginDate = x.BeginDate.ToString(),
                     EndDate = x.EndDate.ToString(),
                     Id = x.Id,
                     PartnerName = x.PartnerName,
                     ServiceInfo = x.ServiceInfo
                 })?.ToList() ?? null,
                Preferences = customer.Preferences?.Select(x =>
                new PreferenceResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                })?.ToList() ?? null
            };

            return customerModel;
        }
        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request">Данные нового клиента</param>
        /// <returns>Id созданного клиента</returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            if (request == null)
                return BadRequest();
            if (request.PreferenceIds != null && !preferences.Any(x => request.PreferenceIds.Any(y => y == x.Id)))
                return NotFound();

            var customer = new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Preferences = new List<Preference>()
            }; 
            var added_customer = await _customerRepository.CreateOrUpdateAsync(customer);

            if (request.PreferenceIds != null && request.PreferenceIds.Any())
                foreach (var pref_id in request.PreferenceIds)
                    customer.Preferences.Add(preferences.First(x => x.Id == pref_id));

            added_customer = await _customerRepository.CreateOrUpdateAsync(customer);

            return Ok(added_customer.Id);
        }
        /// <summary>
        /// Обновить данные о клиенте
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request">Новые данные клиента</param>
        /// <returns>Обновленную информацию о клиенте</returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null|| id == Guid.Empty)
                return BadRequest();

            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            var preferences = await _preferenceRepository.GetAllAsync();
            if (request.PreferenceIds != null && !preferences.Any(x => request.PreferenceIds.Any(y => y == x.Id)))
                return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Preferences = new List<Preference>();

            if (request.PreferenceIds != null && request.PreferenceIds.Any())
                foreach (var pref_id in request.PreferenceIds)
                    customer.Preferences.Add(preferences.First(x=>x.Id == pref_id));

            var updated_customer = await _customerRepository.CreateOrUpdateAsync(customer);

            return await GetCustomerAsync(id);
        }
        /// <summary>
        /// Удалить информацию о клиенте его промокодах и предпочтениях
        /// </summary>
        /// <param name="id">Id удаляемого клиента</param>
        /// <returns>Краткое описание удаленного клиента</returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<CustomerShortResponse>> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            if (id == Guid.Empty)
                return BadRequest();

            var customer = await _customerRepository.DeleteAsync(id);
            if (customer == null)
                return NotFound();

            var customerShort =
                new CustomerShortResponse()
                {
                    Email = customer.Email,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Id = customer.Id
                };

            return customerShort;
        }
    }
}