﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(30)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Description { get; set; }
        public virtual ICollection<Employee> Employees { get; set; }
        public override void Copy(BaseEntity item)
        {
            this.Id = item.Id;
            this.Name = ((Role)item).Name;
            this.Description = ((Role)item).Description;
        }
    }
}