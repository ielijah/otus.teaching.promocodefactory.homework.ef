﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)]
        public string Email { get; set; }

        public Guid RoleId { get; set; }

        [ForeignKey("RoleId")]
        public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }

        public override void Copy(BaseEntity item)
        {
            this.Id = item.Id;
            this.FirstName = ((Employee)item).FirstName;
            this.LastName = ((Employee)item).LastName;
            this.Email = ((Employee)item).Email;
            this.AppliedPromocodesCount = ((Employee)item).AppliedPromocodesCount;
            this.Role.Copy(((Employee)item).Role);
        }
    }
}