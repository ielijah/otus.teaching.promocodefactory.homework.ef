﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)]
        public string Email { get; set; }

        public virtual ICollection<Preference> Preferences { get; set; }
        public virtual ICollection<PromoCode> PromoCodes { get; set; }
        public override void Copy(BaseEntity item)
        {
            this.Id = item.Id;
            this.FirstName = ((Customer)item).FirstName;
            this.LastName = ((Customer)item).LastName;
            this.Email = ((Customer)item).Email;

            //TODO: CheckIT
            this.Preferences = ((Customer)item).Preferences;
            this.PromoCodes = ((Customer)item).PromoCodes;
        }
    }
}