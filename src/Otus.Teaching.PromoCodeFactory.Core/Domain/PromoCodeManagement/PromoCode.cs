﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [MaxLength(30)]
        public string Code { get; set; }

        [MaxLength(30)]
        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        [MaxLength(30)]
        public string PartnerName { get; set; }
        public Guid? PartnerManagerId { get; set; }
        public Guid CustomerId { get; set; }
        public Guid PreferenceId { get; set; }

        public virtual Employee PartnerManager { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual Preference Preference { get; set; }

        public override void Copy(BaseEntity item)
        {
            this.Id = item.Id;
            this.Code = ((PromoCode)item).Code;
            this.ServiceInfo = ((PromoCode)item).ServiceInfo;
            this.BeginDate = ((PromoCode)item).BeginDate;
            this.EndDate = ((PromoCode)item).EndDate;
            this.PartnerName = ((PromoCode)item).PartnerName;

            this.PartnerManager.Copy(((PromoCode)item).PartnerManager);
            this.Customer.Copy(((PromoCode)item).Customer);
            this.Preference.Copy(((PromoCode)item).Preference);
        }
    }
}