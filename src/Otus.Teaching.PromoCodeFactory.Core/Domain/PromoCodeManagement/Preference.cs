﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(30)]
        public string Name { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public override void Copy(BaseEntity item)
        {
            this.Id = item.Id;
            this.Name = ((Preference)item).Name;

            //TODO: CheckIT
            this.Customers = ((Preference)item).Customers;
        }
    }
}