﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public EfContext()
        {
            //Database.EnsureDeleted();
            //Database.EnsureCreated();
            Database.Migrate();
        }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.UseSqlite("Filename=ef.db");
            optionsBuilder.UseLazyLoadingProxies();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
           .Entity<Customer>()
           .HasMany(e => e.PromoCodes)
           .WithOne(e => e.Customer)
           .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);

            modelBuilder.Entity<Customer>()
                    .HasMany(c => c.Preferences)
                    .WithMany(s => s.Customers)
                    .UsingEntity<Dictionary<string, object>>(
                    "CustomerPreference",
                    r => r.HasOne<Preference>().WithMany().HasForeignKey("PreferenceId"),
                    l => l.HasOne<Customer>().WithMany().HasForeignKey("CustomerId"),
                    je =>
                    {
                        je.HasKey("CustomerId", "PreferenceId");
                        je.HasData(
                            new { CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd") },
                            new { CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84") });
                    });
        }
    }
}
