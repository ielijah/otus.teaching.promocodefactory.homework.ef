﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected EfContext Context { get; set; }

        public EfRepository()
        {
            Context = new EfContext();
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            var dbSet = Context.Set<T>();
            return Task.FromResult((IEnumerable<T>)dbSet.Take(100));
        }

        //public Task<IEnumerable<T>> GetAllAsync(string navigationPropertyPath = null)
        //{
        //    if (navigationPropertyPath == null)
        //    {
        //        var dbSet = Context.Set<T>();
        //        return Task.FromResult((IEnumerable<T>)dbSet.Take(100));
        //    }
        //    else
        //    {
        //        var dbSet = Context.Set<T>().Include(navigationPropertyPath);
        //        return Task.FromResult((IEnumerable<T>)dbSet.Take(100));
        //    }
        //}

        public Task<T> GetByIdAsync(Guid id)
        {
            var dbSet = Context.Set<T>();
            return Task.FromResult(dbSet.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> GetByIdAsync(Guid id, string navigationPropertyPath = null)
        {
            if (navigationPropertyPath == null)
            {
                var dbSet = Context.Set<T>().Include(navigationPropertyPath);
                return Task.FromResult(dbSet.FirstOrDefault(x => x.Id == id));
            }
            else
            {
                var dbSet = Context.Set<T>();
                return Task.FromResult(dbSet.FirstOrDefault(x => x.Id == id));
            }
        }
        /// <summary>
        /// Создает новую или обновляет существующую запись
        /// </summary>
        /// <param name="item">экземпляр для добавлекния или обновления</param>
        /// <returns>Возвращает переданное в параметре, если всё ок</returns>
        /// <exception cref="ArgumentNullException">Взрывается, если передали null</exception>
        public Task<T> CreateOrUpdateAsync(T item)
        {
            if (item is null)
                throw new ArgumentNullException(nameof(item));

            if (item.Id == Guid.Empty)
                Create(item);
            else
                Update(item);

            return Task.FromResult(item);
        }
        /// <summary>
        /// Удаляет запись по Id
        /// </summary>
        /// <param name="id">Id записи для удаления</param>
        /// <returns>Возвращает удаленную запись</returns>
        public async Task<T> DeleteAsync(Guid id)
        {
            var item = Context.Find<T>(id);
            if (item != null)
            {
                Context.Remove(item);
                Context.SaveChanges();
            }
            return await Task.FromResult(item);
        }
        /// <summary>
        /// Создает новую запись 
        /// </summary>
        /// <param name="item">новая запись</param>
        private void Create(T item)
        {
            item.Id = Guid.NewGuid();
            Context.Add(item);
            Context.SaveChanges();
        }
        /// <summary>
        /// Обновляет существующую запись или создаёт новую, если не найдено по Id
        /// </summary>
        /// <param name="item">существующая запись</param>
        private void Update(T item)
        {
            Context.Update(item);
            Context.SaveChanges();
        }
    }
}